# -*- coding: utf-8 -*-

import unittest
import httplib
from funkload.FunkLoadTestCase import FunkLoadTestCase


class ServerTest(FunkLoadTestCase):

    def setUp(self):
        self.host = "194.29.175.240"
        self.port = 5000
        self.connection = httplib.HTTPConnection(self.host, self.port)

    def test_folder(self):
        message = '/images'
        expected = '200 OK'
        actual = self.connection.request("GET", message)
        self.assertTrue(expected in self.connection.getresponse())


if __name__ == '__main__':
    unittest.main()
