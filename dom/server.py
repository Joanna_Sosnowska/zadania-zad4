# -*- encoding: utf-8 -*-
__author__ = 'Joanna667'
import socket
import os
import mimetypes
from daemon import runner
import logging

def sciezka():
    return "/home/p14/lab/web"

def http_server(gniazdo):
    s = sciezka()
    while True:
        connection, client_address = gniazdo.accept()
        try:
            try:
                request = connection.recv(1024)
                if request:
                    logger.info("Odebrano:")
                    logger.info(request)
                    address = str(request.decode())
                    print (address)
                    if address[-4:] == "html":
                        try:
                            html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += open(s+address, "r").read()
                            logger.info("Wyslano odpowiedz typu text/html ")
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                            logger.info("404 not found")
                    elif address[-3:] == "txt" or address[-2:] == "py" or address[-3:] == "rst":
                        try:
                            html = "HTTP/1.1 200 OK Content-Type: text/span; charset=UTF-8\r\n\r\n"
                            html += open(s+address, "r").read()
                            logger.info("Wyslano odpowiedz typu text/span ")
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                            logger.info("404 not found")
                    elif address[-3:] == "png":
                        try:
                            html = "HTTP/1.0 200 OK Content-Type: image/png; charset=UTF-8\r\n\r\n"
                            html += '<img src="data:image/png;base64,{0}">'.format(open("web"+address, "rb").read())
                            logger.info("Wyslano odpowiedz typu image/png")
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                            logger.info("404 not found")
                    elif address[-3:] == "jpg":
                        try:
                            html = "HTTP/1.1 200 OK Content-Type: image/jpeg; charset=UTF-8\r\n\r\n"
                            html += '<img src="data:image/jpeg;base64,{0}">'.format(open("web"+address, "rb").read())
                            logger.info("Wyslano odpowiedz typu image/jpg")
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                            logger.info("404 not found")
                    elif not "." in address:
                        print (address)
                        try:
                            print (os.listdir(s+address))
                            html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<!DOCTYPE html><html><head></head><body>"
                            items = os.listdir(s+address)
                            for item in items:
                                print (address+"/"+item)
                                if "." not in item:
                                    item += "/"
                                html += "<a href=\""+item+"\">"+item+"</a><br />"
                            html += "</body></html>"
                            logger.info("Wyslano odpowiedz typu text/html (lista - pliki i foldery)")
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                            logger.info("404 not found")
                    else:
                        try:
                            html = "HTTP/1.1 200 OK Content-Type: plain/text; charset=UTF-8\r\n\r\n"
                            html += open(s+address, 'r').read()
                            logger.info("Wyslano odpowiedz typu plain/text")
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                            logger.info("404 not found")
            except:
                html = "HTTP/1.0 500 Internal Server Error Content-Type: text/html; charset=UTF-8\r\n\r\n"
                html += "<h1>500 Internal Server Error</h1>"
                logger.info("500 Error")
            connection.sendall(html.encode())
        finally:
            connection.close()
            logger.info("Zamkniecie polaczenia")

class App:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/tmp/demonp14.pid'
        self.pidfile_timeout = 5

    # kod aplikacji
    def run(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        server_address = ('0.0.0.0', 5000)
        server_socket.bind(server_address)

        server_socket.listen(5)

        try:
            http_server(server_socket)
        except:
            logger.critical("CRITICAL")
        finally:
            server_socket.close()

logger = logging.getLogger("DemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/home/p14/lab/log/demon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)

if __name__ == '__main__':
    logger.info("Serwer uruchomiony")
    # Stworzenie i uruchomienie aplikacji jako demona
    app = App()
    daemon_runner = runner.DaemonRunner(app)
    # Zapobiegnięcie zamknięciu pliku logów podczas "demonizacji"
    daemon_runner.daemon_context.files_preserve = [handler.stream]
    daemon_runner.do_action()
