# -*- encoding: utf-8 -*-
__author__ = 'Joanna667'
from funkload.FunkLoadTestCase import FunkLoadTestCase
import httplib
import unittest
import os
#import server


#SERVER_HOST = os.environ.get('server', '194.29.175.240:5000')

class ServerTest(FunkLoadTestCase):
    def test_something(self):
        conn = httplib.HTTPConnection("194.29.175.240", 5000)
        conn.request("GET", "/toto")
        res = conn.getresponse()
        self.assertEqual(404, res.status)
if __name__ == '__main__':
    unittest.main()
